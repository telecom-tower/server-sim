package main

// Copyright 2017 Jacques Supcik / Bluemasters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*
Towersimserver implements a web server, receiving frames on a websocket, and
sending back to a WAMP based tower simulator.
*/

//go:generate esc -o html.go -prefix html html

import (
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/geomyidae/ws2811gw"
	"gitlab.com/geomyidae/ws2811sim"
	"gopkg.in/jcelliott/turnpike.v2"
	"html/template"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	wampRealm = "ws2811"
)

func main() {
	var debug = flag.Bool("debug", false, "set debug mode")
	var rows = flag.Int("rows", 8, "LED matrix rows")
	var frequency = flag.Int("frequency", 800, "WS2811 Frequency (in KHz)")
	var columns = flag.Int("columns", 128, "LED matrix columns")
	var port = flag.Int("port", 8484, "HTTP daemon port")
	flag.Parse()

	ledsCount := *rows * *columns

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Debugln("Starting...")

	wampRouter := turnpike.NewBasicWebsocketServer(wampRealm)
	client, err := wampRouter.GetLocalClient(wampRealm, nil)
	if err != nil {
		log.Panic(err)
	}

	r := mux.NewRouter()
	ws2811gw.Init(r, func(r *http.Request) ws2811gw.Ws2811Engine {
		opt := ws2811sim.DefaultOptions
		opt.LedCount = ledsCount
		opt.WampClient = client
		opt.Frequency = uint32(*frequency * 1000)
		return ws2811sim.MakeWS2811(&opt)
	})

	// WAMP router
	r.Handle("/frames", wampRouter)

	// Serve root content (index.html)
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		src, err := FS(false).Open("/index.html")
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		buffer, err := ioutil.ReadAll(src)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		t, err := template.New("index").Parse(string(buffer))
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		t.Execute(w, struct {
			Columns int
			Rows    int
			Url     string
			Realm   string
			Topic   string
		}{
			*columns,
			*rows,
			fmt.Sprintf("ws://localhost:%v/frames", *port),
			wampRealm,
			ws2811sim.WampTopic,
		})
	})

	// Serve static content
	r.PathPrefix("/static/").Handler(http.FileServer(FS(false)))
	r.PathPrefix("/vendors/").Handler(http.FileServer(FS(false)))

	//
	// Start HTTP server
	//
	log.Infof("Starting server on port %v", *port)
	srv := &http.Server{
		Handler:      r,
		Addr:         fmt.Sprintf(":%v", *port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
